#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include "Grade_Do_Jogo.hpp"
#include "Glider.hpp"
#include "Pulsar.hpp"
#include "Glider_Gun.hpp"

using namespace std;

void Menu(){
  system("clear");
  cout << "Welcome to Conway's Game of Life" << endl;
  cout << "Escolha a forma que deseja visualizar:" << endl;
  cout << "[1] Visualizar Glider" << endl;
  cout << "[2] Visualizar Pulsar" << endl;
  cout << "[3] Visualizar Gosper Glider Gun" << endl;
  cout << "[4] Finalizar programa" << endl;
}

void MenuRepeticoes(){
  system("clear");
  cout << "Escolha a quantidade de repeticoes para a forma: (Mínimo de 10)" << endl;
}

void MenuPosicao(){

  system("clear");
  cout << "Deseja escolher posição inicial? (S/s ou N/n)" << endl;

}

int main(){
  char opcao, escolha;
  int repeticoes, i;
  int linha, coluna;

while(1){
  Menu();
  cin >> opcao;

  if(opcao=='4'){
    system("clear");
    cout << "Até a próxima!" << endl;
    return 0;
  }
  if((opcao == '1') || (opcao == '2') || (opcao == '3')){
    MenuRepeticoes();
    cin >> repeticoes;
    while(repeticoes < 10){
      cin >> repeticoes;
    }
  }

  if(opcao=='1'){
    MenuPosicao();
    cin >> escolha;
    if(escolha == 'S' || escolha == 's'){
      system("clear");
      cout << "Digite a linha inicial: (Entre 1 e 28)" << endl;
      cin >> linha;
      while(linha > 28 || linha < 1){
        cin >> linha;
      }
      system("clear");
      cout << "Digite a coluna inicial: (Entre 1 e 32)" << endl;
      cin >> coluna;
      while(coluna > 32 || coluna < 1){
        cin >> coluna;
      }

      Glider aplica;
      aplica.extermina_Matriz();
      aplica.GlideR(linha, coluna);

      for(i=0;i<repeticoes;i++){
        aplica.checar_regras();
        usleep(200000);
        system("clear");
        aplica.imprimir();
      }

    }
    else if(escolha == 'N' || escolha == 'n'){

      Glider aplica;
      aplica.extermina_Matriz();
      aplica.GlideR();

      for(i=0;i<repeticoes;i++){
        aplica.checar_regras();
        usleep(200000);
        system("clear");
        aplica.imprimir();
      }
    }
  }

  else if(opcao=='2'){
    MenuPosicao();
    cin >> escolha;
    if(escolha == 'S' || escolha == 's'){
      system("clear");
      cout << "Digite a linha inicial: (Entre 1 e 18)" << endl;
      cin >> linha;
      while(linha > 18 || linha < 1){
        cin >> linha;
      }
      system("clear");
      cout << "Digite a coluna inicial: (Entre 1 e 22)" << endl;
      cin >> coluna;
      while(coluna > 22 || coluna < 1){
        cin >> coluna;
      }

      Pulsar aplica;
      aplica.extermina_Matriz();
      aplica.PulsaR(linha, coluna);

      for(i=0;i<repeticoes;i++){
        aplica.checar_regras();
        usleep(200000);
        system("clear");
        aplica.imprimir();
      }
    }

      else if(escolha == 'N' || escolha == 'n'){
        Pulsar aplica;
        aplica.extermina_Matriz();
        aplica.PulsaR();

        for(i=0;i<repeticoes;i++){
          aplica.checar_regras();
          usleep(200000);
          system("clear");
          aplica.imprimir();
        }
      }
    }
  else if(opcao=='3'){
    Glidergun aplica;
    aplica.extermina_Matriz();
    aplica.GliderGun();

    for(i=0;i<repeticoes;i++){
      aplica.checar_regras();
      usleep(200000);
      system("clear");
      aplica.imprimir();
    }
  }
}

  return 0;
}
