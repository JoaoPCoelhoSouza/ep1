#include <iostream>
#include <stdlib.h>

#include "Pulsar.hpp"

using namespace std;

Pulsar::Pulsar(){

}

void Pulsar::PulsaR(){

  setMatriz(16,18);
  setMatriz(16,19);
  setMatriz(16,20);
  setMatriz(16,24);
  setMatriz(16,25);
  setMatriz(16,26);
  setMatriz(18,16);
  setMatriz(19,16);
  setMatriz(20,16);
  setMatriz(24,16);
  setMatriz(25,16);
  setMatriz(26,16);
  setMatriz(28,18);
  setMatriz(28,19);
  setMatriz(28,20);
  setMatriz(28,24);
  setMatriz(28,25);
  setMatriz(28,26);
  setMatriz(18,28);
  setMatriz(19,28);
  setMatriz(20,28);
  setMatriz(24,28);
  setMatriz(25,28);
  setMatriz(26,28);
  setMatriz(18,21);
  setMatriz(19,21);
  setMatriz(20,21);
  setMatriz(24,21);
  setMatriz(25,21);
  setMatriz(26,21);
  setMatriz(18,23);
  setMatriz(19,23);
  setMatriz(20,23);
  setMatriz(24,23);
  setMatriz(25,23);
  setMatriz(26,23);
  setMatriz(21,18);
  setMatriz(21,19);
  setMatriz(21,20);
  setMatriz(21,24);
  setMatriz(21,25);
  setMatriz(21,26);
  setMatriz(23,18);
  setMatriz(23,19);
  setMatriz(23,20);
  setMatriz(23,24);
  setMatriz(23,25);
  setMatriz(23,26);

}

void Pulsar::PulsaR(int linha, int coluna){

  setMatriz(linha+3,coluna+5);
  setMatriz(linha+3,coluna+6);
  setMatriz(linha+3,coluna+7);
  setMatriz(linha+3,coluna+11);
  setMatriz(linha+3,coluna+12);
  setMatriz(linha+3,coluna+13);
  setMatriz(linha+5,coluna+3);
  setMatriz(linha+6,coluna+3);
  setMatriz(linha+7,coluna+3);
  setMatriz(linha+11,coluna+3);
  setMatriz(linha+12,coluna+3);
  setMatriz(linha+13,coluna+3);
  setMatriz(linha+15,coluna+5);
  setMatriz(linha+15,coluna+6);
  setMatriz(linha+15,coluna+7);
  setMatriz(linha+15,coluna+11);
  setMatriz(linha+15,coluna+12);
  setMatriz(linha+15,coluna+13);
  setMatriz(linha+5,coluna+15);
  setMatriz(linha+6,coluna+15);
  setMatriz(linha+7,coluna+15);
  setMatriz(linha+11,coluna+15);
  setMatriz(linha+12,coluna+15);
  setMatriz(linha+13,coluna+15);
  setMatriz(linha+5,coluna+8);
  setMatriz(linha+6,coluna+8);
  setMatriz(linha+7,coluna+8);
  setMatriz(linha+11,coluna+8);
  setMatriz(linha+12,coluna+8);
  setMatriz(linha+13,coluna+8);
  setMatriz(linha+5,coluna+10);
  setMatriz(linha+6,coluna+10);
  setMatriz(linha+7,coluna+10);
  setMatriz(linha+11,coluna+10);
  setMatriz(linha+12,coluna+10);
  setMatriz(linha+13,coluna+10);
  setMatriz(linha+8,coluna+5);
  setMatriz(linha+8,coluna+6);
  setMatriz(linha+8,coluna+7);
  setMatriz(linha+8,coluna+11);
  setMatriz(linha+8,coluna+12);
  setMatriz(linha+8,coluna+13);
  setMatriz(linha+10,coluna+5);
  setMatriz(linha+10,coluna+6);
  setMatriz(linha+10,coluna+7);
  setMatriz(linha+10,coluna+11);
  setMatriz(linha+10,coluna+12);
  setMatriz(linha+10,coluna+13);

}
