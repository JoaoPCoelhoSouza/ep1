#ifndef GRADE_DO_JOGO_HPP
#define GRADE_DO_JOGO_HPP

using namespace std;

class Grade {
private:
  bool Matriz[36][40];
  bool Matriz_copia[36][40];
  int linha, coluna;

public:
 Grade ();

 void setMatriz(int linha, int coluna);
 int checar_Vivos(int linha, int coluna);
 void checar_regras();
 void imprimir();
 void copiar_Matriz();
 void extermina_Matriz();
};

#endif
